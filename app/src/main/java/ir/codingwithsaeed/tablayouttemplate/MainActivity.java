package ir.codingwithsaeed.tablayouttemplate;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.tabs.TabLayout;

public class MainActivity extends AppCompatActivity {

    private MaterialToolbar toolbar_main;
    private TabLayout tab_main;
    private ViewPager pager_main;
    private MainPagerAdapter pagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupToolbar();
        setupTabLayout();

        initListeners();
    }

    private void initListeners() {
        tab_main.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 0) {
                    toolbar_main.setBackgroundColor(ContextCompat.getColor(MainActivity.this,
                            R.color.colorPrimary));
                    tab_main.setBackgroundColor(ContextCompat.getColor(MainActivity.this,
                            R.color.colorPrimary));
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        getWindow().setStatusBarColor(ContextCompat.getColor(MainActivity.this,
                                R.color.colorPrimaryDark));
                    }
                    return;
                }

                toolbar_main.setBackgroundColor(ContextCompat.getColor(MainActivity.this,
                        R.color.colorPrimaryYellow));
                tab_main.setBackgroundColor(ContextCompat.getColor(MainActivity.this,
                        R.color.colorPrimaryYellow));
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    getWindow().setStatusBarColor(ContextCompat.getColor(MainActivity.this,
                            R.color.colorPrimaryYellowDark));
                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void setupToolbar() {
        toolbar_main = findViewById(R.id.toolbar_main);
        toolbar_main.setTitleTextColor(Color.WHITE);
        toolbar_main.setTitle(getResources().getString(R.string.app_name));

        setSupportActionBar(toolbar_main);
    }

    private void setupTabLayout() {
        tab_main = findViewById(R.id.tab_main);
        pager_main = findViewById(R.id.pager_main);

        pagerAdapter = new MainPagerAdapter(getSupportFragmentManager(),
                FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);

        pagerAdapter.addItem(new AndroidFragment(), "Android");
        pagerAdapter.addItem(new IosFragment(), "iOS");

        pager_main.setAdapter(pagerAdapter);
        tab_main.setupWithViewPager(pager_main);
    }
}
